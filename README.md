# Behavior Tree Workshop

## Table of contents

- [Behavior Tree Workshop](#behavior-tree-workshop)
  - [Table of contents](#table-of-contents)
  - [Blackboard](#blackboard)
  - [AIController](#aicontroller)
  - [Composites](#composites)
    - [Selector](#selector)
    - [Sequence](#sequence)
    - [Simple parallel](#simple-parallel)
  - [Decorators](#decorators)
  - [Services](#services)
  - [Tasks](#tasks)
  - [Misc](#misc)
- [Exercises](#exercises)

## Blackboard
Separate asset to store information inside **Blackboard Keys**

## AIController
When the controller takes the possession of the pawn it should run the Behavior Tree.

## Composites
### Selector
Executes their children from left to right. They stop executing when one of their children succeeds. If a Selector's child succeeds, the Selector succeeds. If all the Selector's children fail, the Selector fails.

### Sequence
Executes their children from left to right. They stop executing when one of their children fails. If a child fails, then the Sequence fails. If all the Sequence's children succeed, then the Sequence succeeds

### Simple parallel
Allows a single main Task node to be executed alongside of a full tree. When the main Task finishes, the setting in Finish Mode dictates if the node should finish immediately, aborting the secondary tree, or if it should delay for the secondary tree to finish.

## Decorators
Are attached to either a Composite or a Task node and define whether or not a branch in the tree, or even a single node, can be executed.

Observer aborts:
- None
- Self
- Lower priority
- Both

Examples:
- Blackboard
- Composite

``` cpp
/** calculates raw, core value of decorator's condition.*/
virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

/** called when auxiliary node becomes active */
virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

/** register observer for blackboard key */
FDelegateHandle UBlackboardComponent::RegisterObserver(FBlackboard::FKey KeyID, UObject* NotifyOwner, FOnBlackboardChangeNotification ObserverDelegate)

/** request execution change: helpers for decorator nodes */
void UBehaviorTreeComponent::RequestExecution(const UBTDecorator* RequestedBy)

```

## Services
Are attached to Composite or Task nodes and will execute at their defined frequency as long as their branch is being executed. These are often used to make checks and to update the Blackboard

```cpp
virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

/** called when auxiliary node becomes active */
virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

```


## Tasks
Tasks are nodes that "do" things, like move an AI, or adjust Blackboard values

```cpp
/** Starts this task, should return Succeeded, Failed or InProgress */
virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

/** message handler, default implementation will finish latent execution/abortion */
virtual void OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess) override;

/** size of instance memory */
virtual uint16 GetInstanceMemorySize() const override;

```
## Misc
Module dependencies: AIModule


# Exercises
Make an AI with behavior trees with the same behavior as the following state machines:

<details class="mt-1">
  <summary>Exercise 1</summary>

  ![](./images/BTW_ex1.png)
</details>

<details>
  <summary>Exercise 2</summary>

  ![](./images/BTW_ex2.png)
</details>

<details>
  <summary>Help Exercise 1</summary>

  ![](./images/BTW_help_ex1.png)
</details>



