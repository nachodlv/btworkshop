// Copyright Epic Games, Inc. All Rights Reserved.


#include "BTWorkshopGameMode.h"

#include "UObject/ConstructorHelpers.h"

ABTWorkshopGameMode::ABTWorkshopGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/AnimStarterPack/Ue4ASP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
