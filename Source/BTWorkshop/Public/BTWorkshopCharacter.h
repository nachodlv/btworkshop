// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BTWorkshopCharacter.generated.h"

UCLASS(config=Game)
class ABTWorkshopCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	ABTWorkshopCharacter();

	void Shoot();

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(EditAnywhere, Category = "BTW|Shooting", meta = (AllowPrivateAccess = "true"))
	float ShootingRange;

	UPROPERTY(EditAnywhere, Category = "BTW|Shooting", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* ImpactEffect;
};

