﻿#pragma once

#include "CoreMinimal.h"

#include "AIController.h"

#include "BTWAIController.generated.h"

UCLASS()
class BTWORKSHOP_API ABTWAIController : public AAIController
{
	GENERATED_BODY()

public:
	ABTWAIController();

protected:
	virtual void OnPossess(APawn* InPawn) override;

private:
	UPROPERTY(EditAnywhere, Category="BTWAI", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* AIBehavior;
};
