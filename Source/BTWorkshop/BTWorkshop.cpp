// Copyright Epic Games, Inc. All Rights Reserved.

#include "BTWorkshop/BTWorkshop.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BTWorkshop, "BTWorkshop" );
