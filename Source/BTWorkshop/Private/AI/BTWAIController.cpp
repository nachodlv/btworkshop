﻿#include "BTWorkshop/Public/AI/BTWAIController.h"

#include "BehaviorTree/BlackboardComponent.h"

ABTWAIController::ABTWAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABTWAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	RunBehaviorTree(AIBehavior);
	UBlackboardComponent* BlackboardComponent = GetBlackboardComponent();
	if (!BlackboardComponent)
	{
		return;
	}
}
