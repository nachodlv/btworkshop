﻿#include "AI/BehaviorTree/Tasks/BTTask_Shoot.h"

#include "AIController.h"
#include "BTWorkshopCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* AIController = OwnerComp.GetAIOwner();
	ABTWorkshopCharacter* Character = AIController ? Cast<ABTWorkshopCharacter>(AIController->GetPawn()) : nullptr;

	if (Character)
	{
		Character->Shoot();
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
